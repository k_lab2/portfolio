
## NAME OF PROJECT: AUTHOMATIC COIN BASE PUBLIC TOILET PAYMEN
## PROBLEM STATEMENT
Public toilets in urban areas face issues like manual payments, potential germ transmission via infrastructure contact, and uncertain air quality. These challenges impact user experience and hygiene, highlighting the need for automated systems ensuring payment security, minimal physical contact, and optimal air conditions for client well-being

## PROPOSED SOLUTION
 I thought of developing an "Automated coin-based public toilet system" to tackle public restroom problems. The system utilizes embedded technology, opening doors upon coin insertion and monitoring air quality. If air quality falls below acceptable levels, the system intervenes to restore optimal conditions automatically.
## WORKING PRINCIPLE
The coin-operated public toilet system is intricately designed for efficiency and safety. A gas sensor on pin A0 monitors air quality, while servo motors on pins 9 and 10 manage access based on user inputs like 50 or 100. A DC motor at pin 7 aids ventilation during high gas levels, complemented by an LED on pin 6 for notifications. Real-time updates on an LCD ensure user awareness and system reliability, creating a robust automated public toilet solution

## TOOL AND DEVICES NEEDED
Coin acceptor validates coins.
Locking system secures the door.
Sensors monitor air, hygiene, and temperature.
Microcontroller processes sensor data.
LCDs hygiene and status.
Alert system signals with buzzers or LEDs.
Power supply runs system with DC or battery




